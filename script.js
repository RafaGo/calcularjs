var firstNumber;
var secondNumber;
var operation;
var screen = document.getElementById('display');
const hora = document.getElementById('hora');
const music = document.getElementById('music');
const click = document.getElementById('click');


setInterval(function time(){
    let dateToday = new Date();
    let hr = dateToday.getHours();
    let min = dateToday.getMinutes();
    let s = dateToday.getSeconds();

    s = (s < 10) ? "0" + s : s;
    min = (min < 10) ? "0" + min : min;

    hora.textContent = `${hr}:${min}:${s}`;
},1 );

function resetValues(){
    this.firstNumber = '';
    this.secondNumber = '';
    this.operation = '';
}

function cleanDisplay(){
    this.screen.innerText = '';
}

function isFirstNumber(){
    return !this.operation == '';
    
}

function captureNumber(number){  
    click.play();

    if(this.isFirstNumber()){
        this.firstNumber += number;
        this.screen.innerText = this.firstNumber;
    } else {
        this.secondNumber += number;
        this.screen.innerText = this.secondNumber;
    }
}  

function clean(){
    this.resetValues();
    this.cleanDisplay();
} 

function captureOperation(operation){
    click.play();

    this.operation = operation;
    this.cleanDisplay();
}
function calculate() { 
    music.play();

    const body = {
        n1: this.firstNumber,
        n2: this.secondNumber,
        operacao: this.operation,
        resultado:this.screen
    };
    
    const options = {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    };

    fetch('http://localhost:8080/calcula', options)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            const display = document.querySelector('.display');
            display.innerText = data.resultado;
            this.resetValues();
        })
        .catch(error => console.error(error));
}

this.resetValues();